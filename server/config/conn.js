
import mysql from 'mysql';

var con = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'hammerst_hammer',
    multipleStatements: true,
    connectTimeout:30000
});

con.connect((err) =>{
    if(err) throw err;
    else console.log('MySql connected')
});

export default con;