import db from '../../config/conn';
import async from 'async';
import axios from 'axios';
import keys from '../../config/keys';

export const redirectTo = (req,res)=>{
  
    return res.redirect(keys.origin.url);
}

export const getNotification = (req,res)=>{
    if(typeof req.body.order_id !== "undefined" && req.body.order_id !== '' && req.body.order_id.length > 0){


    let queryOrderPayment = `SELECT
    op.payment_type,
    op.order_id,
    op.status_code,
    op.transaction_id,
    op.transaction_status,
    op.transaction_time,
    op.pdf_url,
    os.status as order_status_code
    from order_payment as op
    left join orders as ord on op.order_id = ord.id
    left join order_status as os on ord.order_status_id = os.id 
    where ord.id = ?
    `;

    let queryUpdateStatusOrder = `
    UPDATE orders set orders.order_status_id = (SELECT id from order_status where code = ? limit 1)
    where orders.id = '${req.body.order_id}';
    UPDATE order_payment set status_code = ? , transaction_status = ? 
    where  order_id = '${req.body.order_id}' and transaction_id = ?
    `;


    let queryUpdatePaymentOrder = ` UPDATE order_payment set status_code = ? , transaction_status = ? 
    where  order_id = '${req.body.order_id}' and transaction_id = ?`;

    let queryOrderPaymentInsert = `INSERT into order_payment set ? `;
    async.parallel({
        order_status:function(callback){
            //  FIND ORDER PAYMENT
            db.query(queryOrderPayment, [req.body.order_id],(err, result) => {
    if (err) {
        callback(err, null);
    }
    if (result.length === 0 ||
        (
            result.length > 0
            &&
            (typeof result[0].payment_status_code === "undefined" || result[0].payment_status_code === '' || result[0].payment_status_code === null)
            &&
            (typeof result[0].payment_type === "undefined" || result[0].payment_type === '' || result[0].payment_type === null)
            &&
            (typeof result[0].transaction_id === "undefined" || result[0].transaction_id === '' || result[0].transaction_id === null)
            &&
            (typeof result[0].transaction_status === "undefined" || result[0].transaction_status === '' || result[0].transaction_status === null)
        )
    ) {
        // UPDATE OR INSERT PAYMENT
        axios({
            url: keys.midtrans.url + "/v2/" + req.body.order_id+ "/status",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json	",
            },
            auth: {
                username: keys.midtrans.serverKey,
                password: ""
            }
        }).then(ress => {
            if (ress.data.status_code !== '404') {
                let dataOrderPayment = {};
                Object.keys(ress.data).forEach(rb => {

                    if (
                        rb === "fraud_status" ||
                        rb === "payment_type" ||
                        rb === "finish_redirect_url" ||
                        rb === "status_code" ||
                        rb === "transaction_id" ||
                        rb === "transaction_status" ||
                        rb === "transaction_time" ||
                        rb === "order_id" ||
                        rb === "pdf_url") {

                        dataOrderPayment[rb] = ress.data[rb];
                    }
                    if (rb === "gross_amount") {
                        dataOrderPayment[rb] = parseInt(ress.data[rb]);
                    }
                });
                
                if (Object.keys(dataOrderPayment.length > 0)) {

                    db.query(queryOrderPaymentInsert, [dataOrderPayment], (err, result) => {
                        if (err) {
                            callback(err, null);
                        }
                        if (result) {
                            if (ress.data.status_code.match(/^[4]/g) || ress.data.status_code.match(/^[5]/g)) {
                                db.query(queryUpdateOnlyStatus, ['202', dataOrderPayment.order_id], (err, result) => {
                                    callback(err, 'ok');
                                })
                            } else {
                                callback(null, 'ok');
                            }

                        }
                    });

                } else {
                    callback(null, null);
                }
            } else {
                callback(null, null);
            }
        }).catch(error => {
            callback('error', null);
        })
    }

    if (result.length > 0) {

        // UPDATE PAYMENT
        axios({
            url: keys.midtrans.url + "/v2/" + req.body.order_id + "/status",
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json	",
            },
            auth: {
                username: keys.midtrans.serverKey,
                password: ""
            }
        }).then(ress => {
            //  UPDATE STATUS AND ORDER PAYMENT IF NOT CANCEL
            if (ress.data.status_code !== result[0].status_code
                && ress.data.transaction_status !== "cancel"
                && (!ress.data.status_code.match(/^[4]/g) || !ress.data.status_code.match(/^[5]/g))
                && (ress.data.status_code.match(/^[2]/g) && ress.data.status_code !== '202')
                && result[0].order_status_code !== "ok") {
                db.query(queryUpdateStatusOrder, [
                    ress.data.status_code,
                    ress.data.status_code,
                    ress.data.transaction_status,
                    ress.data.transaction_id], (err, result) => {
                        callback(err, result);
                    })
            }

            // CANCELED BY MIDTRANS
            if (ress.data.status_code !== result[0].status_code
                && result[0].order_status_code !== "ok"
                && (ress.data.transaction_status == "cancel" || ress.data.status_code === '202' || ress.data.status_code.match(/^[4]/g) || ress.data.status_code.match(/^[5]/g))
            ) {
                db.query(queryUpdateStatusOrder, [
                    "202",
                    ress.data.status_code,
                    ress.data.transaction_status,
                    ress.data.transaction_id], (err, result) => {
                        callback(err, result);
                    })
            }


            //EDIT BY ADMIN AND UPDATE STATUS PAYMENT AND ORDER
            if (ress.data.status_code !== result[0].status_code
                && (!ress.data.status_code.match(/^[4]/g) || !ress.data.status_code.match(/^[5]/g))
                && (ress.data.status_code.match(/^[2]/g) && ress.data.status_code !== '202' && ress.data.transaction_status !== "cancel")
                && result[0].order_status_code === "ok") {
                db.query(queryUpdatePaymentOrder, [
                    ress.data.status_code,
                    ress.data.status_code,
                    ress.data.transaction_status,
                    ress.data.transaction_id], (err, result) => {
                        callback(err, result);
                    })
            }
            // EDIT BY ADMIN AND UPDATE STATUS PAYMENT AND ORDER = CANCEL 
            if (ress.data.status_code !== result[0].status_code
                && result[0].order_status_code === "ok"
                && (ress.data.transaction_status === "cancel" || ress.data.status_code === '202' || ress.data.status_code.match(/^[4]/g) || ress.data.status_code.match(/^[5]/g))
            ) {
                db.query(queryUpdatePaymentOrder, [
                    "202",
                    ress.data.status_code,
                    ress.data.transaction_status,
                    ress.data.transaction_id], (err, result) => {
                        callback(err, result);
                    })
            }
            if (ress.data.status_code == result[0].status_code) {
                callback(null, null);
            }
        }).catch(err => {
            callback("ERROR", null);
        });
    }

    })
        }


    },function(err,result){
        if(err){
            return res.status(503).json('ERROR');
        }
        if(result){
            return res.status(200).json('OK');
        }
    })
  }else{
      return res.status(503).json('ERROR');
  }
}







// export const getNotification = (req,res)=>{

//     let queryUpdateStatusOrder = `
//     UPDATE orders set orders.order_status_id = (SELECT id from order_status where code = ? limit 1)
//     where orders.id = ? ;
//     UPDATE order_payment set status_code = ? , transaction_status = ? 
//     where  order_id = ? and transaction_id = ?
//     `;
//     let queryUpdatePaymentOrder = ` UPDATE order_payment set status_code = ? , transaction_status = ? 
//     where  order_id = ? and transaction_id = ? `;
//     let queryUpdateOnlyStatus =`UPDATE orders set orders.order_status_id = (SELECT id from order_status where code = ? limit 1)
//     where orders.id = ?`;
//     let queryOrderPaymentInsert = `INSERT into order_payment set ? `;

//     let querySelectOrder = `
//     select 
// ord.id,
// ord.order_status_id,
// op.fraud_status,
// op.payment_type,
// op.status_code as payment_status_code,
// op.transaction_id,
// op.transaction_status,
// os.status,
// os.code as order_status_code
// from orders as ord 
// left join order_payment as op on ord.id = op.order_id
// left join order_status as os on ord.order_status_id = os.id
// where ord.id = ?
//     `;

//     if(Object.keys(req.body).length > 0 && typeof req.body.order_id !== "undefined"){
//         db.query(querySelectOrder,[req.body.order_id],(err,result)=>{
          
//             if(err){
//                 return res.status(503).json(err); 
//             }
//             if(result.length === 0){
//                 let dataOrderPayment = {};

//                 Object.keys(req.body).forEach(rb => {
//                     if (
//                         rb === "fraud_status" ||
//                         rb === "payment_type" ||
//                         rb === "finish_redirect_url" ||
//                         rb === "status_code" ||
//                         rb === "transaction_id" ||
//                         rb === "transaction_status" ||
//                         rb === "transaction_time" ||
//                         rb === "order_id" ||
//                         rb === "pdf_url") {

//                         dataOrderPayment[rb] = req.body[rb];
//                     }
//                     if (rb === "gross_amount") {
//                         dataOrderPayment[rb] = parseInt(req.body[rb]);
//                     }
//                 });
//                 if (Object.keys(dataOrderPayment.length > 0)) {
//                     async.parallel({
//                         insert:function(callback){
//                             db.query(queryOrderPaymentInsert, [dataOrderPayment], (err, result) => {
//                                 callback(err,result);
//                             });
//                         },
//                         update:function(callback){
//                             if (req.body.status_code.match(/^[4]/g) || req.body.status_code.match(/^[5]/g) ){

//                                 db.query(queryUpdateOnlyStatus, ['202',req.body.order_id], (err, result) => {
//                                     callback(err, result);
//                                     })
//                             }
//                         }
//                     },function(err,result){
//                         if(err){
//                             return res.status(503).json('ERROR');
//                         }
//                         if(result){
//                             return res.status(200).json('ok');
//                         }
//                     })
             
//                 }else{
//                     return res.status(200).json('NONE');
//                 }
//             }
//             if(result.length > 0 
//                 && 
//                 (typeof result[0].payment_status_code !== "undefined" && result[0].payment_status_code !== '' && result[0].payment_status_code !== null ) 
//                 &&
//                 (typeof result[0].payment_type !== "undefined" && result[0].payment_type !== '' && result[0].payment_type !== null)
//                 &&
//                 (typeof result[0].transaction_id !== "undefined" && result[0].transaction_id !== '' && result[0].transaction_id !== null)
//                 &&
//                 (typeof result[0].transaction_status !== "undefined" && result[0].transaction_status !== '' && result[0].transaction_status !== null)

//                 ){
           
//                 if (req.body.status_code !== result[0].payment_status_code
//                     && (req.body.transaction_status !== "cancel" && req.body.transaction_status !== "expired")
//                     && (!req.body.status_code.match(/^[4]/g) || !req.body.status_code.match(/^[5]/g))
//                     && req.body.status_code !== '202'
//                     && (result[0].order_status_code !== "ok" && result[0].order_status_code !== '202')
//                     ) {
                 
//                     db.query(queryUpdateStatusOrder, [
//                         req.body.status_code,
//                         req.body.order_id,
//                         req.body.status_code,
//                         req.body.transaction_status,
//                         req.body.order_id,
//                         req.body.transaction_id], (err, result) => {
//                             if (err) {
//                                 return res.status(503).json(err);
//                             }
//                             if (result) {
//                                 return res.status(200).json('OK1');
//                             }
//                         })
//                 }
//                 if (req.body.status_code !== result[0].payment_status_code
//                     && (req.body.status_code == "202" || req.body.transaction_status === 'cancel' || req.body.status_code.match(/^[4]/g) || req.body.status_code.match(/^[5]/g) )
//                     && (result[0].order_status_code !== "ok"  && result[0].order_status_code !== '202')
//                 ) {
                

//                     db.query(queryUpdateStatusOrder, [
//                         "202",
//                         req.body.order_id,
//                         req.body.status_code,
//                         req.body.transaction_status,
//                         req.body.order_id,
//                         req.body.transaction_id], (err, result) => {
//                             if (err) {
//                                 return res.status(503).json(err);
//                             }
//                             if (result) {
//                                 return res.status(200).json('OK2');
//                             }
//                         })
//                 }

//                 if (req.body.status_code !== result[0].payment_status_code
//                     && (req.body.transaction_status !== "cancel" || req.body.transaction_status !== "expired")
//                     && (!req.body.status_code.match(/^[4]/g) || !req.body.status_code.match(/^[5]/g))
//                     && req.body.status_code !== '202'
//                     && (result[0].order_status_code === "ok" && result[0].order_status_code !== '202')
//                     ) {
                
//                     db.query(queryUpdatePaymentOrder, [
//                         req.body.status_code,
//                         req.body.transaction_status,
//                         req.body.order_id,
//                         req.body.transaction_id], (err, result) => {
//                             if (err) {
//                                 return res.status(503).json(err);
//                             }
//                             if (result) {
//                                 return res.status(200).json('OK3');
//                             }
//                         })
//                 }
//                 if (req.body.status_code !== result[0].payment_status_code
//                     && (req.body.transaction_status == "cancel" || req.body.status_code === '202' || req.body.status_code.match(/^[4]/g) || req.body.status_code.match(/^[5]/g))
//                     && (result[0].order_status_code === "ok" && result[0].order_status_code !== '202')
//                 ) {
           
//                     db.query(queryUpdatePaymentOrder, [
//                         req.body.status_code,
//                         req.body.transaction_status,
//                         req.body.order_id,
//                         req.body.transaction_id], (err, result) => {
//                             if (err) {
//                                 return res.status(503).json(err);
//                             }
//                             if (result) {
//                                 return res.status(200).json('OK4');
//                             }
//                         })
//                 }

//                 if (req.body.status_code == result[0].payment_status_code)  {
//                         return res.status(200).json('OK5');
//                 }
//             }
//             if (result.length > 0
//                 &&
//                 (typeof result[0].payment_status_code === "undefined" || result[0].payment_status_code === '' || result[0].payment_status_code === null)
//                 &&
//                 (typeof result[0].payment_type === "undefined" || result[0].payment_type === '' || result[0].payment_type === null)
//                 &&
//                 (typeof result[0].transaction_id === "undefined" || result[0].transaction_id === '' || result[0].transaction_id === null)
//                 &&
//                 (typeof result[0].transaction_status === "undefined" || result[0].transaction_status === '' || result[0].transaction_status === null)
//             ){
//                 let dataOrderPayment = {};
//                 Object.keys(req.body).forEach(rb => {
//                     if (
//                         rb === "fraud_status" ||
//                         rb === "payment_type" ||
//                         rb === "finish_redirect_url" ||
//                         rb === "status_code" ||
//                         rb === "transaction_id" ||
//                         rb === "transaction_status" ||
//                         rb === "transaction_time" ||
//                         rb === "order_id" ||
//                         rb === "pdf_url") {

//                         dataOrderPayment[rb] = req.body[rb];
//                     }
//                     if (rb === "gross_amount") {
//                         dataOrderPayment[rb] = parseInt(req.body[rb]);
//                     }
//                 });
//                 if (Object.keys(dataOrderPayment.length > 0)) {
//                     async.parallel({
//                         insert: function (callback) {
//                             db.query(queryOrderPaymentInsert, [dataOrderPayment], (err, result) => {
//                                 callback(err, result);
//                             });
//                         },
//                         update: function (callback) {
//                             if (req.body.status_code.match(/^[4]/g) || req.body.status_code.match(/^[5]/g) ) {

//                                 db.query(queryUpdateOnlyStatus, ['202',req.body.order_id], (err, result) => {
//                                     callback(err, result);
//                                 })
//                             }
//                         }
//                     }, function (err, result) {
//                         if (err) {
//                             return res.status(503).json('ERROR');
//                         }
//                         if (result) {
//                             return res.status(200).json('ok');
//                         }
//                     })

//                 } else {
//                     return res.status(200).json('NONE');
//                 }
//             }
//         })
//     }else{
     
//         return res.status(503).json('NO DATA');
//     }

// }