import {Router} from 'express';
import * as v1Controller from './controller';

const routes = new Router();

routes.post('/api/payment/notification',v1Controller.getNotification);
routes.post('/api/payment/redirect',v1Controller.redirectTo);
export default routes;