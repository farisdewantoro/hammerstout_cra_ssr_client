import axios from 'axios';
import keys from '../../config/keys';

export const getMediaRecent = (req,res)=>{
    axios.get(`https://api.instagram.com/v1/users/self/media/recent/?access_token=${keys.instagram.access_token}`)
        .then(result=>{
            let imageInstagram = result.data.data.filter((d,i)=> i < 10).map(r=>{
                return{
                    images:r.images,
                    caption:r.caption,
                    link:r.link
                }
                  
                
            });
        
            return res.status(200).json(imageInstagram);
        })
        .catch(err=>{
            return res.status(400).json(err);
        });

}