import {Router} from 'express';
import * as InstagramController from './controller';

const routes = new Router();

routes.get('/instagram/get/media/recent',InstagramController.getMediaRecent);


export default routes;