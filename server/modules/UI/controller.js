import db from '../../config/conn';
import async from 'async';


export const getContentHome = (req,res)=>{
    let querySelectSlider = `SELECT 
    si.id,i.public_id,i.link,i.caption,i.alt,i.tag
    from slider_image as si
    left join images as i on si.image_id = i.id`;
    let querySelectLookbook = `SELECT lb.name,lb.slug,lb.id as lookbook_id,i.public_id,i.link,i.caption,i.alt,i.tag,i.height,i.width,i.size 
from lookbooks as lb
left join lookbook_image as li on li.id = (SELECT li1.id from lookbook_image as li1 where lb.id = li1.lookbook_id order by li1.id asc limit 1)
left join images as i on i.id = (SELECT i1.id from images as i1 where li.image_id = i1.id order by i1.id asc limit 1)
group by lb.name,i.public_id,i.link,i.caption,i.alt,i.tag
order by lb.created_at desc limit 2`;
   let querySelectCategories = `SELECT 
   c.name as category,
   c.slug as category_slug,
   ctt.name as category_tag
   from categories as c
   left join category_attribute as ca on c.id = ca.category_id
   left join category_tag as ctt on ca.category_tag_id = ctt.id`;
   let querySelectCategoryTag = `SELECT 
   ctt.name as category_tag, ctt.slug as category_tag_slug
   from category_tag as ctt order by ctt.created_at asc`;
   let querySelectCategoryType = `SELECT
   ct.name as category_type,
   ct.slug as category_type_slug,
   c.name as category
   from category_type as ct
   left join categories as c on ct.category_id = c.id`;
   let querySelectCollection = `SELECT
   c.name,
   c.slug,
   c.id as collection_id,
   i.public_id,i.link,i.caption,i.alt,i.tag,
   i.id as image_id
   from collections as c 
   left join collection_image as ci on ci.id = (SELECT ci1.id from collection_image as ci1 where ci1.collection_id = c.id order by ci1.id asc limit 1)
   left join images as i on ci.image_id = i.id 
   order by c.created_at desc `;

   let querySelectColor = `select distinct original_color,hex_color from product_variant `;
async.parallel({
    slider:function(callback){
        db.query(querySelectSlider,(err,result)=>{
            callback(err,result);
        })
    },
    lookbook:function(callback){
        db.query(querySelectLookbook,(err,result)=>{
            callback(err,result);
        })
    },
    category:function(callback){
        db.query(querySelectCategories,(err,result)=>{
            callback(err,result)
        })
    },
    category_tag:function(callback){
        db.query(querySelectCategoryTag,(err,result)=>{
            callback(err,result)
        })
    },
    category_type: function (callback) {
        db.query(querySelectCategoryType, (err, result) => {
            callback(err, result)
        })
    },
    collection:function(callback){
        db.query(querySelectCollection,(err,result)=>{
            callback(err,result);
        })
    },
    product_color:function(callback){
        db.query(querySelectColor,(err,result)=>{
            callback(err,result);
        })
    }
},function(err,result){
    if(err) return res.status(400).json(err);
    if(result){
        return res.status(200).json(result);
    }
})

}
