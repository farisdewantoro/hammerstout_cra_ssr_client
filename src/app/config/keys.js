export default {
    loginWith:{
        google:"http://localhost:5000/api/auth/google",
        facebook:"http://localhost:5000/api/auth/facebook"
    },
    jwt: {
        secretOrPrivateKey: "AAAABB3L-X59kbcuqwzxc--23kv,df594.41239zsc92231",
        secretOrPrivateKey2: "2DCtkGVe-jifmqs53v6sbgg05u3fkbcDuDxDDqwzD2xc--23kv,df594.0ut79s41qi0lhg",
        secretOrPrivateKey3: "X59kbcuqwzxc-VcMFhXkPjVaIjz--23kv,df594.df594xxx",
        expiresIn: '2d'
    },
}