import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import Navigator from './Navigator';
import CartList from './CartList';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import { updatedCart, deletecart} from '../../actions/cartActions';
import CartTotal from './CartTotal';
import { setVoucherDiscount} from '../../actions/voucherActions';
import Hidden from '@material-ui/core/Hidden';
import {withRouter} from 'react-router';
import Page from '../../components/page';
 class Carts extends Component {
     constructor(props){
        super(props);
        this.state={
            carts:[],
            voucher:''
        }
     }
  componentDidMount(){
 
      this.setState({
          carts: this.props.carts.cartList
      })
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.carts !== this.props.carts){
        this.setState({
            carts: nextProps.carts.cartList
        })
    }
  }
     handlerChangeQuantity=(i)=>(e)=>{
         this.props.updatedCart({ cart_items_id: this.state.carts[i].cart_items_id,quantity:e.target.value});
     }
     handlerDeleteCart=(i)=>{
     
         this.props.deletecart(this.state.carts[i].cart_items_id);
     }
     handlerChangeVoucher = (e)=>{
        this.setState({
            voucher:e.target.value
        })
     }
     handlerSubmitVoucher =()=>{
         if (!this.props.auths.isAuthenticated){
            this.props.history.push('/sign-in');
         }else{
             if (this.state.voucher !== '') {
                 this.props.setVoucherDiscount({ code: this.state.voucher });
             }
         }
        
        
     }


  render() {
      const { classes, vouchers} = this.props;
      const {loading} = this.props.carts;
      const {voucher,carts} = this.state;
    //   let carts = [{
    //       alt: "hammerstout",
    //       caption: "hammerstout",
    //       cart_items_id: 119,
    //       cart_status: 1,
    //       category_name: "Sweatshirts",
    //       category_type: "Pullover Hoodies",
    //       category_type_slug: "pullover-hoodies",
    //       description: "<p>Type: Pullover Hoodie<br />Material: Cotton Fleece<br />Color: Black<br />READY SIZE<br />S , M , L , XL&nbsp;<br />Price: 305.000</p>",
    //       discount_percentage: null,
    //       discount_value: null,
    //       link: "https://res.cloudinary.com/hammerstout/image/upload/v1545650387/SWEATSHIRT/STENCIL1.jpg",
    //       product_attribute_id: 34,
    //       product_id: 14,
    //       product_name: "STENCIL",
    //       product_slug: "stencil",
    //       product_variant_id: 16,
    //       quantity: 1,
    //       regular_price: 290000,
    //       size: "M",
    //       slug: "sweatshirts",
    //       stock: 2
    //   }];
      if(carts.length > 0){
          return (
              <Page
                id="Carts"
                title="Carts"
              >
              <div className={classes.rootCarts}>
                  
                  <Grid container direction="column">
                      <Grid item xs={12}>
                          <Grid container justify="center">
                              <Hidden smDown>
                                  <Navigator />
                              </Hidden>
                             


                              <Grid item md={10}>

                                  <Grid container direction="row" spacing={40}>
                                      <Grid item md={7} xs={12}>
                                          <CartList
                                              carts={carts}
                                              handlerChangeQuantity={this.handlerChangeQuantity}
                                              handlerDeleteCart={this.handlerDeleteCart} />
                                      </Grid>
                                      <Grid item md={5} xs={12}>
                                          <CartTotal
                                              handlerChangeVoucher={this.handlerChangeVoucher}
                                              handlerSubmitVoucher={this.handlerSubmitVoucher}
                                              carts={carts}
                                              voucher={voucher}
                                              vouchers={vouchers}
                                          />
                                      </Grid>
                                  </Grid>

                              </Grid>
                          </Grid>
                      </Grid>
                  </Grid>
                  <Dialog fullScreen open={loading}>

                      <Grid container justify="center" alignItems="center" direction="column" style={{ height: "100%" }}>

                          <CircularProgress className={classes.progress} />
                          <Typography className={classes.normalText} style={{ margin: "20px 0" }}>Please wait Loading..</Typography>
                      </Grid>

                  </Dialog>
              </div>
              </Page>
          )
      }
     if(carts.length === 0){
         return(
             <div className={classes.rootCarts}>
                 <Grid container justify="center" direction="column" alignItems="center" spacing={40}>
                 
                     <Grid item>
                         <Grid container justify="center" style={{ margin: "20px 0" }}>
                         {/* <Navigator /> */}
                          </Grid>
                     </Grid>
                     <Grid item >
                         <Typography variant="h1" className={classes.titleParamsActive}>
                             Your cart is currently empty.
                        </Typography>
                        <Grid container justify="center" style={{margin:"20px 0"}}>
                             <Button variant="contained" component={Link} to="/shop" color="primary">
                                 RETURN TO SHOP
                             </Button>
                        </Grid>
                       
                     </Grid>
               
                 </Grid>
             </div>
       
         )
     }else{
         return(
             <p> NO CART </p>
         )
     }
    
  }
}

Carts.propTypes = {
    products: PropTypes.object.isRequired,
    carts: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    updatedCart:PropTypes.func.isRequired,
    deletecart:PropTypes.func.isRequired,
    auths:PropTypes.object.isRequired,
    setVoucherDiscount:PropTypes.func.isRequired,
    vouchers:PropTypes.object.isRequired

}

const mapStateToProps = state => (
    {
        products: state.products,
        carts: state.carts,
        auths:state.auths,
        vouchers:state.vouchers
    });

export default compose(
    connect(mapStateToProps, { updatedCart, deletecart, setVoucherDiscount}),
    withStyles(styles, { name: "Carts" }))(withRouter(Carts));
