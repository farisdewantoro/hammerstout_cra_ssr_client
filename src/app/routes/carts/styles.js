export default theme=>({
    rootCarts: {
        [theme.breakpoints.up('md')]:{
            padding: "50px 0"
        },
        [theme.breakpoints.down('sm')]:{
            padding:"5px 10px"
        }
      
    },
    titleParams: {
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.5em",
        color: 'rgba(0, 0, 0, 0.26)'
    },
    titleParamsActive: {
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.5em"
    },
    navigatorArrowDisabled:{
        color:'rgba(0, 0, 0, 0.26)'
    },
    productTitle: {
        fontFamily: "'Staatliches', cursive",
        fontSize: '1.5em',
        lineHeight: 1.5

    },
    productType: {
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans- serif",
        fontSize: '1em'
    }, productPricing: {
        marginTop: 10
    },
    regular_price: {
        fontFamily: "'Staatliches', cursive",
        [theme.breakpoints.down('sm')]: {
            fontSize: 16,
        },
        [theme.breakpoints.up('md')]: {
            fontSize: 20,
        },

        marginRight: 5
    },
    normalText:{
        fontFamily: "'Staatliches', cursive",
        fontSize:16,
        color:"#484848"
       
    },
    isDiscount: {
        textDecoration: 'line-through',
        [theme.breakpoints.down('sm')]: {
            fontSize: 12,
        },
        [theme.breakpoints.up('md')]: {
    fontSize: 16,
        },
        color: '#484848'
    },
    discount_value: {
        color: '#e53935',
        fontFamily: "'Staatliches', cursive",
        [theme.breakpoints.down('sm')]: {
            fontSize: 16,
        },
        [theme.breakpoints.up('md')]: {
            fontSize: 20,
        },
   

    },
    discountPercentage: {
        color: '#e53935',
        fontFamily: "'Staatliches', cursive",
        fontSize: 20
    },
    noDiscount: {
        opacity: 0,
        fontFamily: "'Staatliches', cursive",
        fontSize: 16
    },
    productDiscountPricing: {
        display: 'flex',
        alignItems: 'center'
    },
    productDescription: {
        margin: '10px 0px 10px 0px'
    },
    WraperProductPricing:{
        display:'flex',
        alignItems:"center"
    },
    quantityWrapper:{
        display: 'flex',
        alignItems: "center",
        margin:'10px 0px'
    },
    selectQuantity:{
        marginLeft:5,
    padding: "5px 45px 5px 10px",
    lineHeight: "1.4",
    border:" 1px solid #dadada",
    borderRadius: 0,
    backgroundColor: "white",
    // color: "#1a1a1a",
        fontFamily: "'Staatliches', cursive",
        fontSize: 16,
        color: "#484848",
    margin: 0
    },
    textProductAttribute:{
        marginLeft: 5,
    },
    inputVoucher:{
        width:'100%',
        // color: "#1a1a1a",
        fontFamily: "'Staatliches', cursive",
        fontSize: 16,
        color: "#484848",
        padding:'4px 5px',
        border:"none",
        boxShadow: "0px 0px 1px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), -1px 0px 0px 0px rgba(0,0,0,0.12)",
        outline:"none"
    }
   
})