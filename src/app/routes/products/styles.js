export default theme=>({
    rootProducts:{
        [theme.breakpoints.up('md')]:{
            paddingTop:50
        },
        // [theme.breakpoints.down('sm')]:{
        //     padding: 10,
        // }
    },
    containerGridProduct: {
        [theme.breakpoints.up('md')]: {
            marginTop: theme.spacing.unit * 3
        },
        [theme.breakpoints.down('sm')]: {
            margin:0,
        }
       
    },
    mobileFilter:{
        margin:"10px 18px 0px 18px"
    },
    wrapperSearchbox:{
        [theme.breakpoints.down('sm')]: {
            marginBottom:"20px",
        }
    },
    appBarMobile: {
        position:"relative"
    },
    AppBarBottomMobile:{
        top: 'auto',
        bottom: 0,
    },
    rootPaper: {
        padding: '2px',
        display: 'flex',
        alignItems: 'center',
        width: '100%',
    },

    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        width: 1,
        height: 28,
        margin: 4,
    },
    titleParams: {
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.5em"
    },
    productDetail: {
        margin: theme.spacing.unit
    },
    productTitle: {
        fontFamily: "'Staatliches', cursive",
        fontSize: 18

    },
    productType: {
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans- serif",
        fontSize: 12
    },
    productPricing: {
        width: '100%'
    },
    regular_price: {
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans- serif",
        fontSize: 15,
        marginRight: 5,
    },
    isDiscount: {
        textDecoration: 'line-through',
        fontSize: 13,
        color: '#484848'
    },
    discount_value: {
        color: '#e53935',
        fontFamily: "'Staatliches', cursive",
        fontSize: 16

    },
    discountPercentage: {
        color: '#e53935',
        fontFamily: "'Staatliches', cursive",
        fontSize: 16
    },
    noDiscount: {
        opacity: 0,
        fontFamily: "'Staatliches', cursive",
        fontSize: 16
    },
    titleBigProduct: {
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        padding: "20px 0 20px 0",
        fontSize: "2.5em"
    },



    root: {
        flexGrow: 1,
        maxWidth: 752
    },
    demo: {
        backgroundColor: theme.palette.background.paper
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`
    },
    subHeaderList: {
        display: 'flex',
        justifyContent: 'space-between'
    },
    titleSublist: {
        padding: "8px",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans- serif",
        fontWeight: 500,
        textTransform: "Uppercase",
        letterSpacing: "0.02857em",
        lineHeight: 1.5,
        minWidth: "64px",
        color: "rgba(0, 0, 0, 0.87)",
        fontSize: "0.875rem",
        boxSizing: "border-box",
        minHeight: "36px"
    },
    rootListColorFilter: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
    },
    boxColor: {
        width: 15,
        height: 15,
        border: " 2px solid white",
        boxShadow: "0px 0px 0px 1px #9c9b9b87"
    },
    marginLeftRight: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit
    },
    containerBoxColor: {
        display: 'flex',
    },
    titleMenuList:{
        fontFamily: "'Staatliches', cursive",
        fontSize:"1.3em"
    },
    parentMenuList:{
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.2em"
    },
    childMenuList:{
    fontSize: "1rem",
    },
    childchildList:{
        fontSize:"0.9em",
        padding:"0px 10px",
        color:"black"
    },
    childWrapper:{
        padding:"0px 10px"
    },

    listItemButton:{
        padding:"5px 10px"
    },
    nested:{
        padding:"5px 0px",
    },
   
})