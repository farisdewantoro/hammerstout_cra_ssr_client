import React, { Component } from 'react'
import HomeBanner from './HomeBanner'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import TodayHighlight from './TodayHighlight'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { getProductWithParamCategory } from '../../actions/productActions';
import Collection from './Collection';
import Lookbook from './Lookbook';
import {ImageFull} from '../../components/loader/Loader';
import ReactPixel  from 'react-facebook-pixel';
import InstagramFeed from './instagramFeed';
import Page from '../../components/page';
const styles = theme => ({
    tes:{
        height:'200vh'
    }
});

class Home extends Component {
componentDidMount(){

    ReactPixel.pageView(); 
   
}
UNSAFE_componentWillReceiveProps(nextProps){
    if (nextProps.UI.category !== this.props.UI.category) {
        nextProps.UI.category.unshift({
            category: "New Arrivals",
            category_slug: "newarrivals",
            category_tag: "none"
        })
    }
}
  render() {
      const { UI, instagrams } = this.props;
      
    return (
        <Page
            id="homepage"
            title="Handcrafted Denim Products "
        >
            <div >
                {UI.slider.length > 0 ? (
                    <HomeBanner image={UI.slider} />
                ) : (
                        <ImageFull />
                    )}

                <Grid style={{ padding: 10 }}  >

                    <Collection collection={UI.collection} />


                    <Lookbook lookbook={UI.lookbook} />




                    <TodayHighlight
                        category={UI.category}
                        getProductWithParamCategory={this.props.getProductWithParamCategory}
                        products={this.props.products} />

                    <InstagramFeed
                        instagrams={instagrams}
                    />

                </Grid>


            </div>
        </Page>
     
    )
  }
}

Home.propTypes = {
    getProductWithParamCategory: PropTypes.func.isRequired,
    products: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    UI:PropTypes.object.isRequired,
    instagrams:PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    products: state.products,
    UI:state.UI,
    instagrams:state.instagrams
})

export default
    compose(
        connect(mapStateToProps, { getProductWithParamCategory }),
        withStyles(styles, { name: 'Home' })
    )(Home);