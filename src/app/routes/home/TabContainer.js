import React,{Component} from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import { Link } from 'react-router-dom'
import styles from '../products/styles'

function formatCurrency(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
class TabContainer extends Component {
    constructor(props){
        super(props);
        this.state={
            products:[]
        }
    }
    componentDidMount(){
        let propsIndex = this.props.tabIndex;
        let menuList = this.props.menu;
      
        this.props.getProductWithParamCategory(menuList[propsIndex].category_slug);
    }
 
    componentWillReceiveProps(nextProps){
        let nextTabIndex = nextProps.tabIndex;
        let menuList = this.props.menu;
        if (this.props.tabIndex !== nextProps.tabIndex){
            this.props.getProductWithParamCategory(menuList[nextTabIndex].category_slug);
        }
        if (this.props.products.todayHighlight !== nextProps.products.todayHighlight){
            this.setState({
                products: nextProps.products.todayHighlight
            })
        }
      
    }
    render(){
        let {products} = this.state;
        let { classes} = this.props;
     


    return (
        <div className={classes.containerGridProduct}>
         
                <Grid container direction="row" spacing={16}>
               
                {products.map((p, i) => {
                    return (
                        <Grid item md={3} xs={6} key={i}>
                            <Card>
                                <CardActionArea component={Link} to={`/products/${p.category_slug}/${p.product_id}-${p.slug}`}>
                                    <img src={p.link} alt={p.alt} title={p.caption} style={{ width: '100%' }} />
                                    <div className={classes.productDetail}>

                                        <div style={{ marginBottom: 10 }}>
                                            <Typography className={classes.productTitle}>
                                                {p.name}
                                            </Typography>
                                            <Typography className={classes.productType}>
                                                {p.category_type}
                                            </Typography>

                                        </div>


                                        <div className={classes.productPricing}>

                                            <span className={classNames(classes.regular_price, {
                                                [classes.isDiscount]: (p.discount_value !== null && p.discount_percentage !== null)
                                            })}>
                                                {`IDR ${formatCurrency(p.regular_price)}`}
                                            </span>
                                        </div>
                                        {(p.discount_value !== null && p.discount_percentage !== null ?
                                            <div >
                                                <Grid container direction="row" justify="space-between">
                                                    <Typography component="p" className={classes.discount_value}>
                                                        {`IDR ${formatCurrency(p.discount_value)}`}
                                                    </Typography>
                                                    <Typography component="p" className={classes.discountPercentage}>
                                                        {p.discount_percentage} % OFF
                                                </Typography>
                                                </Grid>


                                            </div>
                                            :
                                            <div >
                                                <Typography component="p" className={classes.noDiscount}>
                                                    NO DISCOUNT
                                </Typography>
                                            </div>
                                        )}

                                    </div>


                                </CardActionArea>
                            </Card>

                        </Grid>
                    )
                })}
                    
                </Grid>
            
        </div>
    )
    }
 
}

TabContainer.propTypes = {
    tabIndex: PropTypes.number.isRequired,
    menu: PropTypes.array.isRequired,
    getProductWithParamCategory:PropTypes.func.isRequired,
    products:PropTypes.object.isRequired,
    classes:PropTypes.object.isRequired
};



export default  withStyles(styles,{name:'TabContainer'})(TabContainer);
