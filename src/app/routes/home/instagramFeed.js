import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import withWidth from '@material-ui/core/withWidth';
import {compose} from 'redux';
class InstagramFeed extends Component {
    state={
        image:[]
    }

    // componentWillReceiveProps(nextProps){
    //     if(nextProps.instagrams.instagram !== this.props.instagrams.instagram){
    //         this.setState({
    //             image: nextProps.instagrams.instagram.map(())
    //         })
    //     }
    // }
    render() {
        const { classes, instagrams,width} = this.props;
        return (
            <div>
                <Grid item xs={12} style={{
                    marginTop: 100
                }}>
                    <Grid container justify="center">
                        <Grid item xs={11}>
                            <Grid container >
                                <div style={{ width: "100%" }}>
                                    <Typography variant="h1" className={classes.titleInstagram}>
                                        @hammerstoutdenim
                                 </Typography>
                                </div>
                                <div className={classes.rootInstagram}>
                                    <GridList cellHeight={260} className={classes.gridList} cols={width === 'xs' || width === 'sm' ? 2:5 }>
                                        {instagrams.instagram.map((tile,i) => (
                                            <GridListTile key={tile.images.standard_resolution.url} >
                                            
                                                    <img src={tile.images.standard_resolution.url} style={{cursor:'pointer',maxWidth:'auto'}} onClick={()=>window.open(tile.link,'_blank')} alt={tile.caption.text} title='HAMMER STOUT DENIM ®' />
                                     
                                            </GridListTile>
                                        ))}
                                    </GridList>
                                </div>
                            </Grid>
                     
                        </Grid>


                    </Grid>

                </Grid>
            </div>
        )
    }
}

InstagramFeed.propTypes = {
    classes: PropTypes.object.isRequired,
    width: PropTypes.string.isRequired,
}

export default compose(withStyles(styles), withWidth())(InstagramFeed);
