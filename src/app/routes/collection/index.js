import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import { getAllCollection } from '../../actions/collectionActions';
import Page from '../../components/page';
class Collection extends Component {
    componentDidMount(){
        this.props.getAllCollection();
    }
    render() {
        const { classes } = this.props;
        const { collection } = this.props.collections;
        let cString = collection.map(c=>{
           return c.name
        }).toString();
        return (
            <Page
                id="Collection"
                title={`${cString} collections `}
            >
                <div className={classes.paddingRoot}>
                    <Grid container >
                        <Grid item md={12}>
                            <Grid container justify="center">
                                <Grid item md={10}>
                                    <Typography className={classes.titleCollection}>
                                        Collection
                                </Typography>
                                    <Grid container direction="row" justify="center" spacing={16}>

                                        {collection.map((co, i) => {
                                            return (
                                                <Grid item md={12} key={i} >
                                                    <Link to={`/collection/${co.slug}`}>
                                                        <img src={co.link} style={{ width: "100%" }} alt={co.alt} />
                                                    </Link>

                                                </Grid>
                                            )
                                        })}

                                    </Grid>

                                </Grid>
                            </Grid>

                        </Grid>

                    </Grid>
                </div>
            </Page>
        
        )
    }
}

Collection.propTypes = {
    classes: PropTypes.object.isRequired,
    collections: PropTypes.object.isRequired,
    getAllCollection: PropTypes.func.isRequired
}

const mapStateToProps = state => ({ collections: state.collections });

export default compose(connect(mapStateToProps, { getAllCollection }), withStyles(styles, { name: "Collection" }))(Collection);
