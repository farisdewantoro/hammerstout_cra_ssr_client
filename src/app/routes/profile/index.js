import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography'
import styles from './styles';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import moment from 'moment';
import {updateProfile} from '../../actions/myAccounts';
import Page from '../../components/page';
class Profile extends Component {
    state={
        user:{
            firstname:"",
            lastname:"",
            displayName:"",
            gender:""
        },
        email:"",
        user_information:{
            birthday:new Date(),
            phone_number:""
        },
      
    }

    handlerChangeUser =(e)=>{
        let name = e.target.name;
        let value = e.target.value;
        this.setState(prevState=>({
            user:{
                ...prevState.user,
                [name]:value
            }
        }))
    }
    componentDidMount(){
        let stateUser = this.state.user;
        let stateUserInfo = this.state.user_information;
        let user = this.props.auths.user;
     
        if (Object.keys(user).length > 0){
            Object.keys(stateUser).forEach(keys => {
                Object.keys(user).forEach(userKey => {
                    this.setState(prevState => ({
                        user: {
                            ...prevState.user,
                            [keys]: user[keys]
                        }
                    }))
                })
            });
            Object.keys(stateUserInfo).forEach(keys => {
                Object.keys(user).forEach(userKey => {

                    this.setState(prevState => ({
                        user_information: {
                            ...prevState.user_information,
                            [keys]: user[keys]
                        }
                    }))
                })
            });
            this.setState({
                email: user.email
            })

        }
    }
    UNSAFE_componentWillReceiveProps(nextProps){
        let user = nextProps.auths.user;
        let stateUser = this.state.user;
        let stateUserInfo = this.state.user_information;
        if(user !== this.props.auths.user){
            Object.keys(stateUser).forEach(keys => {
                Object.keys(user).forEach(userKey=>{
                    this.setState(prevState => ({
                        user:{
                            ...prevState.user,
                            [keys]:user[keys]
                        }
                    }))
                })
            });
            Object.keys(stateUserInfo).forEach(keys => {
                Object.keys(user).forEach(userKey => {
                    this.setState(prevState => ({
                        user_information: {
                            ...prevState.user_information,
                            [keys]: user[keys]
                        }
                    }))
                })
            });
            this.setState({
                email:user.email
            })
        }
 
    }
    handlerChangeUserInfo = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState(prevState => ({
            user_information: {
                ...prevState.user_information,
                [name]: value
            }
        }))
    }
    handlerUpdateProfile = ()=>{
        let { user, user_information, email} = this.state;
        this.props.updateProfile({ user, user_information, email });
    }
    render() {
        const { classes,errors } = this.props;
        const {firstname,lastname,displayName,gender} = this.state.user;
        const {birthday,phone_number} = this.state.user_information;
        return (
            <Page
                id="Profile"
                noCrawl
            >
                
            <div className={classes.wrapperProfile}>
                <Grid container direction="column">
                    <Grid item xs={12}>
                        <Grid container justify="center">
                            <Grid item md={8} xs={12}>
                                <Card>
                                    <Typography variant="h1" className={classes.titleParams} style={{margin:20}}>
                                        Profile
                                    </Typography>
                                    <Divider/>
                                    <CardContent>
                                        <Grid container justify="space-between" style={{width:'100%'}} spacing={8}>
                                            <Grid item md={6}>
                                                <TextField 
                                                label="First Name"
                                                name="firstname"
                                                value={firstname}
                                                fullWidth
                                                error={errors.firstname ? true:false}
                                                helperText={errors.firstname ? errors.firstname : ''}
                                                margin="normal"
                                                onChange={this.handlerChangeUser}
                                                InputLabelProps={{
                                                    shrink: true,
                                                    classes: {
                                                        root: classes.textLabel
                                                    }
                                                }}
                                                />

                                            </Grid>
                                            <Grid item md={6}>
                                                <TextField
                                                    label="Last Name"
                                                    name="lastname"
                                                    value={lastname}
                                                    error={errors.lastname ? true : false}
                                                    helperText={errors.lastname ? errors.lastname : ''}
                                                    fullWidth
                                                    onChange={this.handlerChangeUser}
                                                    margin="normal"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                        classes: {
                                                            root: classes.textLabel
                                                        }
                                                    }}
                                                />
                                            </Grid>
                                        </Grid>

                                        <Grid>
                                            <TextField
                                                label="displayName"
                                                name="displayName"
                                                value={displayName}
                                                fullWidth
                                                error={errors.displayName ? true : false}
                                                helperText={errors.displayName ? errors.displayName : ''}
                                                onChange={this.handlerChangeUser}
                                                margin="normal"
                                                InputLabelProps={{
                                                    shrink: true,
                                                    classes: {
                                                        root: classes.textLabel
                                                    }
                                                }}
                                            />
                                        </Grid>

                                        <Grid>
                                            <TextField
                                                label="Email Address"
                                                name="email"
                                                value={this.state.email}
                                                fullWidth
                                                margin="normal"
                                                error={errors.email ? true : false}
                                                helperText={errors.email ? errors.email : ''}
                                                InputLabelProps={{
                                                    shrink: true,
                                                    classes: {
                                                        root: classes.textLabel
                                                    }
                                                }}
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                            />
                                        </Grid>
                                        <Grid>
                                            <TextField
                                                label="Phone Number"
                                                name="phone_number"
                                                onChange={this.handlerChangeUserInfo}
                                                value={phone_number}
                                                fullWidth
                                                error={errors.phone_number ? true : false}
                                                helperText={errors.phone_number ? errors.phone_number : ''}
                                                margin="normal"
                                                InputLabelProps={{
                                                    shrink: true,
                                                    classes: {
                                                        root: classes.textLabel
                                                    }
                                                }}
                                             
                                            />
                                        </Grid>
                                    <Grid container direction="row"  spacing={40}>
                                            <Grid item xs={12}>
                                                <FormControl component="fieldset" style={{ margin: "10px 0" }}>
                                                    <FormLabel component="legend" error={errors.gender ? true : false} className={classes.textFormLabel}>Gender</FormLabel>
                                                    <FormHelperText error={errors.gender ? true:false}>{errors.gender}</FormHelperText>
                                                    <RadioGroup
                                                        aria-label="Gender"
                                                        name="gender"
                                                        value={gender}
                                                        onChange={this.handlerChangeUser}
                                                    >
                                                        <FormControlLabel value="female" name="gender" control={<Radio color="primary" />} label="Female" />
                                                        <FormControlLabel value="male" name="gender" control={<Radio color="primary" />} label="Male" />
                                                        <FormControlLabel value="other" name="gender" control={<Radio color="primary" />} label="Other" />


                                                    </RadioGroup>
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <TextField
                                                    id="date"
                                                    label="Birthday"
                                                    type="date"
                                                    name="birthday"
                                                    error={errors.birthday ? true:false}
                                                    helperText={errors.birthday ? errors.birthday :''}
                                                    onChange={this.handlerChangeUserInfo}
                                                    className={classes.textFieldBirthDay}
                                                    value={moment(birthday).format("YYYY-MM-DD")}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                        classes: {
                                                            root: classes.textLabel
                                                        }
                                                    }}

                                                />
                                            </Grid>
                                       

                                          
                                    </Grid>
                                   

                                        <Grid style={{margin:"30px 0"}}>
                                            <Button variant="contained" fullWidth color="primary" onClick={this.handlerUpdateProfile}>
                                                    Save Changes
                                            </Button>
                                        </Grid>
                                    </CardContent>

                                </Card>

                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

            </div>

            </Page>
        )
    }
}
Profile.propTypes = {
    auths: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    updateProfile:PropTypes.func.isRequired,
    errors:PropTypes.object.isRequired
}

const mapStateToProps = state => (
    {
        auths: state.auths,
        errors:state.errors
    });

export default compose(connect(mapStateToProps, { updateProfile}), withStyles(styles, { name: "Profile" }))(Profile);
