import { emphasize } from '@material-ui/core/styles/colorManipulator';
export default theme=>({
    rootCheckout: {
        padding:"50px 0"
    },
    titleParams: {
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.5em",
        color: 'rgba(0, 0, 0, 0.26)'
    },
    titleParamsActive:{
        textAlign: 'center',
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.5em"
    },
    navigatorArrowDisabled: {
        color: 'rgba(0, 0, 0, 0.26)'
    },
    titleBilling:{
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.5em",
        margin:15
    },
    selectStyles: {
        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
    },
    root: {
        flexGrow: 1,
        height: 250,
    },
    input: {
        display: 'flex',
        padding: 0,
    },
    valueContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        alignItems: 'center',
        overflow: 'hidden',
    },
    chip: {
        margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
            0.08,
        ),
    },
    noOptionsMessage: {
        padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
    },
    singleValue: {
        fontSize: 16,
    },
    placeholder: {
        position: 'absolute',
        left: 2,
        fontSize: 16,
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: 0,
        right: 0,
    },
    divider: {
        height: theme.spacing.unit * 2,
    },
    textLabel: {
        fontFamily: "'Staatliches', cursive",
        fontSize: "1.2em",
    },
    textLabelSelect:{
        fontFamily: "'Staatliches', cursive",
        fontSize: "15px"
    },
    titleOrder:{
        fontFamily: "'Staatliches', cursive",
        color:"#616161de",
        fontSize: "16px"
    },
    productListCart:{
        display:"flex",
        alignItems: "center"
    },
    normalText: {
        fontFamily: "'Staatliches', cursive",
        fontSize: 16,
        color: "#484848"

    },
})