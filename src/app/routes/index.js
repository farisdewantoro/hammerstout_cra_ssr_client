import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AuthenticatedRoute from '../components/authenticated-route';
import UnauthenticatedRoute from '../components/unauthenticated-route';
import Loadable from 'react-loadable';
import NotFound from './not-found';
import AuthRoute from '../components/common/AuthRoute';
import PrivateRoute from '../components/common/PrivateRoute';
import PrivateRouteOrder from '../components/common/PrivateRouteParams';
const Homepage = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ './home'),
  loading: () => null,
  modules: ['home']
});
const Products = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ './products'),
  loading: () => null,
  modules: ['products']
});
const ProductDetail = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ './productDetail'),
  loading: () => null,
  modules: ['productDetail']
});
const Collection = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './collection'),
  loading: () => null,
  modules: ['collection']
});


const Lookbook = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './lookbook'),
  loading: () => null,
  modules: ['lookbook']
});

const DetailLookbook = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './lookbook/detailLookbook'),
  loading: () => null,
  modules: ['detailLookbook']
});

const ReturnPolicy = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './returnPolicy'),
  loading: () => null,
  modules: ['returnPolicy']
});
const SizeGuide = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './size'),
  loading: () => null,
  modules: ['size']
});
const PaymentGuide = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './paymentGuide'),
  loading: () => null,
  modules: ['paymentGuide']
});
const Help = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './help'),
  loading: () => null,
  modules: ['help']
});
const Carts = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './carts'),
  loading: () => null,
  modules: ['carts']
});
const Checkout = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './checkout'),
  loading: () => null,
  modules: ['checkout']
});
const Payment = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './payment'),
  loading: () => null,
  modules: ['payment']
});
const Login = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './auth/Login'),
  loading: () => null,
  modules: ['Login']
});
const Register = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './auth/Register'),
  loading: () => null,
  modules: ['Register']
});
const MyAccount = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './myAccount'),
  loading: () => null,
  modules: ['myAccount']
});
const Profile = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './profile'),
  loading: () => null,
  modules: ['profile']
});
const Address = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './address'),
  loading: () => null,
  modules: ['address']
});
const Order = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './order'),
  loading: () => null,
  modules: ['order']
});
const OrderDetail = Loadable({
  loader: () => import(/* webpackChunkName: "dashboard" */ './order/detailOrder'),
  loading: () => null,
  modules: ['detailOrder']
});



export default () => (

  <Switch>
      <Route exact path="/" component={Homepage} />
      <Route exact path="/shop" component={Products} />
      <Route path="/redirect" exact render={(props) => (
      <Homepage  {...props} />
      )} />


    <Route path="/carts" exact render={(props) => (
      <Carts {...props} />
    )} />

    <Route path="/shop/:tag" exact render={(props) =>
      (<Products key={props.match.params.tag} {...props} />)
    } />
    <Route path="/shop/:tag/:category" exact render={(props) =>
      (<Products key={props.match.params.category} {...props} />)
    } />
    <Route path="/shop/:tag/:category/:type" exact render={(props) =>
      (<Products key={props.match.params.type} {...props} />)
    } />
    <Route path="/products/:category/:name" exact render={(props) =>
      (<ProductDetail key={props.match.params.name} {...props} />)
    } />


    <Route path="/lookbook" exact render={(props) =>
      (<Lookbook {...props} />)} />
    <Route path="/lookbook/detail/:slug" exact render={(props) =>
      (<DetailLookbook key={props.match.params.slug} {...props} />)} />


    <Route path="/collection" exact render={(props) =>
      (<Collection {...props} />)} />
    <Route path="/collection/:collection" exact render={(props) =>
      (<Products key={props.match.params.collection}  {...props} />)} />

    <Route path="/return-policy" exact render={(props) =>
      (<ReturnPolicy  {...props} />)} />
    <Route path="/size-guide" exact render={(props) =>
      (<SizeGuide  {...props} />)} />
    <Route path="/payment-guide" exact render={(props) =>
      (<PaymentGuide  {...props} />)} />
    <Route path="/help" exact render={(props) => (
      <Help {...props} />
    )} />


    <PrivateRoute path="/checkout" exact component={Checkout}
    />
    <PrivateRouteOrder path="/checkout/:token_order" exact component={Payment}
    />


    <AuthRoute path="/sign-in" exact component={Login} />
    <AuthRoute path="/sign-up" exact component={Register} />


    <PrivateRoute path="/my-account" exact component={MyAccount} />
    <PrivateRoute path="/my-account/profile" exact component={Profile} />
    <PrivateRoute path="/my-account/address" exact component={Address} />
    <PrivateRoute path="/my-account/orders" exact component={Order} />
    <PrivateRouteOrder path="/my-account/orders/detail/:token_order" exact component={OrderDetail} />

  <Route component={NotFound} />
  </Switch>
);
