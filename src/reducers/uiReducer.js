import { GET_CONTENT_HOME, LOADING_UI } from '../app/actions/types';
const initialState = {
    slider:[],
    lookbook:[],
    category:[],
    category_tag:[],
    category_type:[],
    collection:[],
    product_color:[]
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOADING_UI:
            return{
                ...state,
                loading:true
            }
        case GET_CONTENT_HOME:
            return{
                ...state,
                loading:false,
                slider:action.payload.slider,
                lookbook:action.payload.lookbook,
                category:action.payload.category,
                category_tag:action.payload.category_tag,
                category_type:action.payload.category_type,
                collection:action.payload.collection,
                product_color:action.payload.product_color
            }
        default:
            return state;
    }
}