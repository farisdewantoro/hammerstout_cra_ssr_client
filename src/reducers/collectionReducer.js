import { GET_ALL_COLLECTION, LOADING_COLLECTION, GET_DETAIL_COLLECTION } from '../app/actions/types';
const initialState = {
    loading:false,
    collection:[]
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOADING_COLLECTION:
            return{
                ...state,
                loading:true
            };
        case GET_ALL_COLLECTION:
            return {
                ...state,
                loading:false,
                collection:action.payload
            };
        case GET_DETAIL_COLLECTION:
            return{
                ...state,
                loading:false,
                collection: action.payload
            }
        default:
            return state;
    }
}